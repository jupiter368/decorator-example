
function simpleDecor(yourClass) {
  console.log(yourClass);
  yourClass.annotated = true;
  
}

function classDecor(yourClass) {
  return class newDecor extends yourClass {
    getLast() {
      return 'last';
    }
  }
}

// function funcDecor() {
  // console.log('pre func decor');
function inFuncDecor(target, key, descriptor) {
    console.log('target', target);
    console.log('key', key);
    console.log('descriptor', descriptor);
    const original = descriptor.value || descriptor.initializer.call(this);
    console.log('original', original());
    return descriptor;
  }
// }



// @classDecor
class MyClass {
  id = Math.random();

  @inFuncDecor
  getName() {
    return `doh + ${this}`;
  }

  @inFuncDecor
  title =  'abcde';


}

const c1 = new MyClass(1,2,3);
console.log(c1.title);

