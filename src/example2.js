function newConstructor(HumanClass) {
	return class NewClass extends HumanClass {
		constructor(firstName, lastName, age) {
			super();
			this.firstName = firstName
			this.lastName = lastName
			this.age = age
		}
	}
}


@newConstructor
class  Human {
	constructor( firstName, lastName ) {
		this.firstName  =  firstName;
		this.lastName  =  lastName;
	}
	getName() {
		return 'doh';
	}
}

// Though Human class constructor function takes only two parameters, but due to 
// newConstructor now Human class can accept 3 parameters
var  person1  =  new  Human("Virat", "Kohli", 31);
console.log( person1 );

// Displays the modified constructor function
console.log(person1.getName() );
